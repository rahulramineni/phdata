-- using schema USER_RAHULRAMINENI.DATABRICKS
-- To explore all raw data and reports stored by spark processes from databricks

select * from AIRLINES;

select * from AIRPORTS;

select * from FLIGHTS limit 10;

-- Total number of flights by airline and airport on a monthly basis
select * from REPORT_FLIGHTS_BY_MONTH;

-- On time percentage of each airline for the year 2015
select * from REPORT_FLIGHTS_ON_TIME;

-- Airlines with largest number of delays
select * from REPORT_AIRLINE_DELAYS;

-- Cancellation Reasons by Airport
select * from REPORT_FLIGHT_CANCELLATION;

-- Delay Reasons by Airport
select * from REPORT_AIRPORT_DELAY;

-- Airline with most unique routes
select * from REPORT_AIRLINE_ROUTES;
select * from REPORT_UNIQUE_AIRLINE_ROUTES;