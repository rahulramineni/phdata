// Databricks notebook source
// MAGIC %md
// MAGIC #### Steps to load flight data to snowflake tables

// COMMAND ----------

// MAGIC %md
// MAGIC Snowflake Database / Schema : USER_RAHULRAMINENI / DATABRICKS

// COMMAND ----------

val options = Map(
"sfUrl" -> "******.ap-south-1.aws.snowflakecomputing.com", // replace this with your own connection information
"sfUser" -> "******",
"sfPassword" -> "******",
"sfDatabase" -> "USER_RAHULRAMINENI",
"sfSchema" -> "DATABRICKS",
"sfWarehouse" -> "INTERVIEW_WH"
)

// COMMAND ----------

// MAGIC %md
// MAGIC Schema Definitions

// COMMAND ----------

import org.apache.spark.sql.Row;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.apache.spark.sql.types.StringType;
import org.apache.spark.sql.types.DataTypes._;

val bigDecimal = IntegerType
val smallDecimal = createDecimalType(9,6)

val airlineSchema = StructType(Array(
  StructField("IATA_CODE", StringType, false),
  StructField("AIRLINE", StringType, true)))

val airportSchema = StructType(Array(
  StructField("IATA_CODE", StringType, false),
  StructField("AIRPORT", StringType, true),
  StructField("CITI", StringType, true),
  StructField("STATE", StringType, true),
  StructField("COUNTRY", StringType, true),
  StructField("LATITUDE", smallDecimal, true),
  StructField("LONGITUDE", smallDecimal, true)))

val flightSchema = StructType(Array(
  StructField("YEAR", IntegerType, false),
  StructField("MONTH", IntegerType, true),
  StructField("DAY", IntegerType, true),
  StructField("DAY_OF_WEEK", IntegerType, true),
  StructField("AIRLINE", StringType, true),
  StructField("FLIGHT_NUMBER", StringType, true),
  StructField("TAIL_NUMBER", StringType, true),
  StructField("ORIGIN_AIRPORT", StringType, true),
  StructField("DESTINATION_AIRPORT", StringType, true),
  StructField("SCHEDULED_DEPARTURE", StringType, true),
  StructField("DEPARTURE_TIME", StringType, true),
  StructField("DEPARTURE_DELAY", bigDecimal, true),
  StructField("TAXI_OUT", bigDecimal, true),
  StructField("WHEELS_OFF", StringType, true),
  StructField("SCHEDULED_TIME", bigDecimal, true),
  StructField("ELAPSED_TIME", bigDecimal, true),
  StructField("AIR_TIME", bigDecimal, true),
  StructField("DISTANCE", bigDecimal, true),
  StructField("WHEELS_ON", StringType, true),
  StructField("TAXI_IN", bigDecimal, true),
  StructField("SCHEDULED_ARRIVAL", bigDecimal, true),
  StructField("ARRIVAL_TIME", StringType, true),
  StructField("ARRIVAL_DELAY", bigDecimal, true),
  StructField("DIVERTED", bigDecimal, true),
  StructField("CANCELLED", bigDecimal, true),
  StructField("CANCELLATION_REASON", StringType, true),
  StructField("AIR_SYSTEM_DELAY", bigDecimal, true),
  StructField("SECURITY_DELAY", bigDecimal, true),
  StructField("AIRLINE_DELAY", bigDecimal, true),
  StructField("LATE_AIRCRAFT_DELAY", bigDecimal, true),
  StructField("WEATHER_DELAY", bigDecimal, true)
))

// COMMAND ----------

// MAGIC %md
// MAGIC Load AIRLINES

// COMMAND ----------

val airlineDf = spark.read.format("csv")
.option("header", "true")
.schema(airlineSchema)
.load("/FileStore/tables/upload/airlines.csv")

airlineDf.persist
airlineDf.show(3)

airlineDf.write.format("snowflake")
.options(options)
.option("dbtable", "airlines")
.mode("overwrite")
.save()

// COMMAND ----------

// MAGIC %md
// MAGIC Load AIRPORTS

// COMMAND ----------

val airportDf = spark.read.format("csv")
.option("header", "true")
.schema(airportSchema)
.load("/FileStore/tables/upload/airports.csv")

airportDf.persist
airportDf.show(3)

airportDf.write.format("snowflake")
.options(options)
.option("dbtable", "airports")
.mode("overwrite")
.save()

// COMMAND ----------

// MAGIC %md
// MAGIC Load FLIGHTS

// COMMAND ----------

val flightDf = spark.read.format("csv")
.option("header", "true")
.schema(flightSchema)
.load("/FileStore/tables/flights/")

flightDf.persist
flightDf.show(3)

flightDf.write.format("snowflake")
.options(options)
.option("dbtable", "flights")
.mode("overwrite")
.save()



// COMMAND ----------

// MAGIC %md
// MAGIC End of Data Load Process
