// Databricks notebook source
// MAGIC %md
// MAGIC ### Reports

// COMMAND ----------

// MAGIC %md
// MAGIC #####Load Flights Data & Lookup Airport and Airline info

// COMMAND ----------

// MAGIC %md
// MAGIC ######Snowflake Database / Schema : USER_RAHULRAMINENI / DATABRICKS

// COMMAND ----------

val options = Map(
"sfUrl" -> "*****.ap-south-1.aws.snowflakecomputing.com", // replace this with your own connection information
"sfUser" -> "*****",
"sfPassword" -> "******",
"sfDatabase" -> "USER_RAHULRAMINENI",
"sfSchema" -> "DATABRICKS",
"sfWarehouse" -> "INTERVIEW_WH"
)

// COMMAND ----------

val flights = spark.read.format("snowflake").options(options).option("dbtable", "FLIGHTS").load()
val airlines = spark.read.format("snowflake").options(options).option("query", "SELECT IATA_CODE AS AIRLINE, AIRLINE AS NAME FROM AIRLINES").load()
val airports = spark.read.format("snowflake").options(options).option("query", "SELECT IATA_CODE AS ORIGIN_AIRPORT, AIRPORT AS NAME FROM AIRPORTS").load()
airports.persist
val flightsEnriched = flights
  .join(airlines, Seq("AIRLINE"), "left_outer").withColumn("AIRLINE", $"NAME").drop("NAME")
  .join(airports, Seq("ORIGIN_AIRPORT"), "left_outer").withColumn("ORIGIN_AIRPORT", $"NAME").drop("NAME")
  .join(airports.withColumnRenamed("ORIGIN_AIRPORT","DESTINATION_AIRPORT"), Seq("DESTINATION_AIRPORT"), "left_outer")
        .withColumn("DESTINATION_AIRPORT", $"NAME").drop("NAME")

flightsEnriched.persist
flightsEnriched.show(3, false)
printf("Proceeding to generate reports with %d rows \n", flightsEnriched.count)

// COMMAND ----------

// MAGIC %md
// MAGIC #####1. Total number of flights by airline and airport on a monthly basis

// COMMAND ----------

import org.apache.spark.sql.functions._
val flightsMonthly = flightsEnriched.filter($"cancelled"===0)
    .groupBy("year", "month", "airline", "origin_airport").agg(count("*").as("Total_Number_Of_Flights"))
    .sort($"YEAR", $"MONTH")
flightsMonthly.persist
flightsMonthly.sort($"Total_Number_Of_Flights".desc).show(5, false)
flightsMonthly.write.format("snowflake").options(options).option("dbtable", "report_flights_by_month").mode("overwrite").save()

// COMMAND ----------

// MAGIC %md
// MAGIC #####2. On time percentage of each airline for the year 2015
// MAGIC An arrival or departure delay upto 15 minutes is still considered on-time
// MAGIC 
// MAGIC A flight is considered on-time only when both departure and arrival are on-time
// MAGIC 
// MAGIC Cancelled flights are excluded from these numbers

// COMMAND ----------

val flightsOnTime = flightsEnriched
    .filter($"year"===lit(2015) && ($"cancelled"===0 || $"cancelled".isNull))
    .withColumn("OnTime", when(($"DEPARTURE_DELAY" > 15) || ($"ARRIVAL_DELAY" > 15), lit(0)).otherwise(lit(1)))

val flightsOnTimeAgg = flightsOnTime
    .groupBy("AIRLINE").agg(round((lit(100)*sum("onTime")/count("onTime")),2).as("On_Time_Percentage"))
    .sort($"On_Time_Percentage".desc)

flightsOnTimeAgg.persist()
flightsOnTimeAgg.show(5, false)
flightsOnTimeAgg.write.format("snowflake").options(options).option("dbtable", "report_flights_on_time").mode("overwrite").save()
flightsOnTimeAgg.unpersist

// COMMAND ----------

// MAGIC %md
// MAGIC #####3. Airlines with largest number of delays

// COMMAND ----------

val airlineDelays = flightsEnriched.filter($"cancelled"===0)
    .groupBy("AIRLINE")
    .agg(count(expr("IF(AIRLINE_DELAY>0,1,NULL)")).as("NUMBER_OF_DELAYS"),
        count("*").as("NUMBER_OF_FLIGHTS"))
    .withColumn("Percentage_Of_Delayed_Flights", round(expr("100 * NUMBER_OF_DELAYS / NUMBER_OF_FLIGHTS"),2))
    .sort($"NUMBER_OF_DELAYS".desc)

airlineDelays.persist
airlineDelays.show(5, false)
airlineDelays.write.format("snowflake").options(options).option("dbtable", "report_airline_delays").mode("overwrite").save()
airlineDelays.unpersist

// COMMAND ----------

// MAGIC %md
// MAGIC #####4. Cancellation Reasons by Airport

// COMMAND ----------

val flightCancellation = flightsEnriched
    .groupBy("ORIGIN_AIRPORT")
    .agg(count(expr("IFF(CANCELLATION_REASON='A',1,NULL)")).as("DUE_TO_AIRLINE"),
        count(expr("IFF(CANCELLATION_REASON='B',1,NULL)")).as("DUE_TO_WEATHER"),
        count(expr("IFF(CANCELLATION_REASON='C',1,NULL)")).as("DUE_TO_AIR_SYSTEM"),
        count(expr("IFF(CANCELLATION_REASON='D',1,NULL)")).as("DUE_TO_SECURITY"),
        count(expr("IFF(CANCELLATION_REASON IS NOT NULL,1,NULL)")).as("TOTAL_NUM_OF_CANCELLATIONS"),
        count("*").as("TOTAL_NUM_OF_FLIGHTS"))
    .withColumn("Percentage_Of_Cancelled_Flights", round(expr("100 * TOTAL_NUM_OF_CANCELLATIONS / TOTAL_NUM_OF_FLIGHTS"),2))
    .sort($"TOTAL_NUM_OF_CANCELLATIONS".desc)

flightCancellation.persist
flightCancellation.show(5)
flightCancellation.write.format("snowflake").options(options).option("dbtable", "report_flight_cancellation").mode("overwrite").save()
flightCancellation.unpersist

// COMMAND ----------

// MAGIC %md
// MAGIC #####5. Delay Reasons by Airport
// MAGIC Arrival and Departure delays are considered only when they are beyond 15 minutes
// MAGIC 
// MAGIC All other delays are considered whenever they are reported

// COMMAND ----------

import org.apache.spark.sql.types.StringType;
import org.apache.spark.sql.types.DataTypes._;
val originDelay = flightsEnriched.filter($"CANCELLED"===0)
    .select($"origin_airport".as("airport"), $"departure_delay", $"security_delay", $"late_aircraft_delay"
          , $"taxi_out", lit(0).as("arrival_delay").cast(LongType), lit(0).as("taxi_in"))

val destinaitionDelay = flightsEnriched.filter($"CANCELLED"===0)
    .select($"destination_airport".as("airport"), lit(0).as("departure_delay"), lit(0).as("security_delay"), lit(0).as("late_aircraft_delay")
          , lit(0).as("taxi_out"), $"arrival_delay".cast(LongType), $"taxi_in")

val airportDelay = originDelay.union(destinaitionDelay)
    .groupBy("airport")
    .agg(count(expr("IFF(departure_delay > 15, 1, null)")).as("departure_delays"),
        count(expr("IFF(security_delay > 0, 1, null)")).as("security_delays"),
        count(expr("IFF(late_aircraft_delay > 0, 1, null)")).as("late_aircraft_delays"),
        count(expr("IFF(taxi_out > 0, 1, null)")).as("taxi_out_delays"),
        count(expr("IFF(arrival_delay > 15, 1, null)")).as("arrival_delays"),
        count(expr("IFF(taxi_in > 0, 1, null)")).as("taxi_in_delays"))
    .withColumn("Total_Num_of_Delays", expr("departure_delays + security_delays + late_aircraft_delays + taxi_out_delays + arrival_delays + taxi_in_delays"))
    .sort($"Total_Num_of_Delays".desc)
   
airportDelay.persist
airportDelay.show(5)
airportDelay.write.format("snowflake").options(options).option("dbtable", "report_airport_delay").mode("overwrite").save()
airportDelay.unpersist

// COMMAND ----------

// MAGIC %md
// MAGIC #####6. Airline with most unique routes
// MAGIC 
// MAGIC A given combination of "Origin to Destination" is considered a Route (One-Way)

// COMMAND ----------

val airlineAndRoutes = flightsEnriched
    .selectExpr("AIRLINE", "ORIGIN_AIRPORT", "DESTINATION_AIRPORT")
    .dropDuplicates
airlineAndRoutes.persist
airlineAndRoutes.show(5, false)

val uniqueRoutes = airlineAndRoutes
    .groupBy("ORIGIN_AIRPORT", "DESTINATION_AIRPORT")
    .agg(count("*").as("Number_of_Airlnes"))
    .filter($"Number_of_Airlnes"===1)

val airlinesWithUniqueRoutes = airlineAndRoutes
    .join(uniqueRoutes, Seq("ORIGIN_AIRPORT", "DESTINATION_AIRPORT"), "inner")
    .groupBy("AIRLINE")
    .agg(count("*").as("NUMBER_OF_UNIQUE_ROUTES"))
    .sort($"NUMBER_OF_UNIQUE_ROUTES".desc)

airlinesWithUniqueRoutes.persist
airlinesWithUniqueRoutes.show(5, false)
airlinesWithUniqueRoutes.write.format("snowflake").options(options).option("dbtable", "report_UNIQUE_AIRLINE_ROUTES").mode("overwrite").save()
airlineAndRoutes.write.format("snowflake").options(options).option("dbtable", "report_AIRLINE_ROUTES").mode("overwrite").save()
airlinesWithUniqueRoutes.persist

// COMMAND ----------

// MAGIC %md
// MAGIC End of Analysis
